$(document).ready(function(){
    
    $('.btn-more').on('click', function(){
        $(this).next().slideToggle()
    })

    // Переключение самовывоза и доставки в корзине
    $('.cart__self').on('click', function(){
        $(".cart-form__self").fadeOut().next().fadeIn()
    })
    $('.cart__delivery').on('click', function(){
        $(".cart-form__delivery").fadeOut().prev().fadeIn()
    })

    // Стрелка рядом с названием раздела на странице каталог
    $(".title-arrow").on("click", function(){
        $(this).toggleClass("title-arrow__active")
        $(this).parent().next().slideToggle()
    })

    // Модальное окно
    $(".btn-checkout").on("click", function(){
        $(".modal-wrapper").fadeIn()
    })
    $(".navigation-call").on("click", function(){
        $(".modal-wrapper").fadeIn()
    })
    $(".modal-close").on("click", function(){
        $(".modal-wrapper").fadeOut()
    })


    // Скроллинг по нажатию на якорную ссылку
    $("a").click(function() {
        $("html, body").animate({
           scrollTop: $($(this).attr("href")).offset().top + "px"
        }, {
           duration: 500,
           easing: "swing"
        });
        return false;
     });

})