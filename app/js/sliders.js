$(document).ready(function(){
	// Header slider
	$('.header-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		swipe: true,
		arrows: false,
		dots: true,
		autoplay:false,
		fade: true,
		cssEase: 'ease',
		speed: 500,
		

	})
	
	//Slider add_accessories
	$('.slider.add_accessories').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: true,
		centerPadding: '60px',
		swipe: false,
		prevArrow: "<div class='arrow prev'><img src='<?php bloginfo('template_directory') ?>/img/arrow-prev.svg' alt=''></div>",
		nextArrow: "<div class='arrow next'><img src='<?php bloginfo('template_directory') ?>/img/arrow-next.svg' alt=''></div>",

	});
	$('.add_accessories .slick-center').next().addClass("slider-center__next")
	$('.add_accessories .slick-arrow').on('click', function(){
		$('.add_accessories .slick-center').removeClass("slider-center__next").next().addClass("slider-center__next")
	})

	// Slider device
	 $('.device-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		// asNavFor: '.device-slider__nav',
		centerMode: true,
		prevArrow: "<div class='arrow prev'><img src='<?php bloginfo('template_directory') ?>/img/arrow-device-prev.svg' alt=''></div>",
		nextArrow: "<div class='arrow next'><img src='<?php bloginfo('template_directory') ?>/img/arrow-device-next.svg' alt=''></div>",
		// fade: true,
	   });
	   $('.device-slider__nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.device-slider',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		swipe: false
	   });
	$('.device-slider__nav .slider-item').on('click', function(){
		$(this).addClass('slider-item__active').siblings().removeClass('slider-item__active')
	})

	// New-slider
	$('.new-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		centerMode: true,
		swipe: false,
		centerPadding: '60px',
		prevArrow: "<div class='arrow prev'><img src='<?php bloginfo('template_directory') ?>/img/arrow-prev.svg' alt=''></div>",
		nextArrow: "<div class='arrow next'><img src='<?php bloginfo('template_directory') ?>/img/arrow-next.svg' alt=''></div>",
	})

})


